//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BookStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class catagory
    {
        public catagory()
        {
            this.Books = new HashSet<Books>();
        }
    
        public int Ca_id { get; set; }
        public string Cname { get; set; }
    
        public virtual ICollection<Books> Books { get; set; }
    }
}
