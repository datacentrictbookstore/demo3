
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/05/2017 21:17:43
-- Generated from EDMX file: D:\Book Store\BookStore\BookStore\Models\BookER.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BookStoreDATA];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'BooksSet'
CREATE TABLE [dbo].[BooksSet] (
    [Book_id] int IDENTITY(1,1) NOT NULL,
    [Bname] nvarchar(max)  NOT NULL,
    [Author_Au_id] int  NOT NULL,
    [catagory_Ca_id] int  NOT NULL,
    [Publisher_Pub_id] int  NOT NULL
);
GO

-- Creating table 'MemberSet'
CREATE TABLE [dbo].[MemberSet] (
    [Mem_id] int IDENTITY(1,1) NOT NULL,
    [User] nvarchar(max)  NOT NULL,
    [Pass] nvarchar(max)  NOT NULL,
    [Mname] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'catagorySet'
CREATE TABLE [dbo].[catagorySet] (
    [Ca_id] int IDENTITY(1,1) NOT NULL,
    [Cname] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AuthorSet'
CREATE TABLE [dbo].[AuthorSet] (
    [Au_id] int IDENTITY(1,1) NOT NULL,
    [Aname] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PublisherSet'
CREATE TABLE [dbo].[PublisherSet] (
    [Pub_id] int IDENTITY(1,1) NOT NULL,
    [Pname] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'BooksMember'
CREATE TABLE [dbo].[BooksMember] (
    [Books_Book_id] int  NOT NULL,
    [Member_Mem_id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Book_id] in table 'BooksSet'
ALTER TABLE [dbo].[BooksSet]
ADD CONSTRAINT [PK_BooksSet]
    PRIMARY KEY CLUSTERED ([Book_id] ASC);
GO

-- Creating primary key on [Mem_id] in table 'MemberSet'
ALTER TABLE [dbo].[MemberSet]
ADD CONSTRAINT [PK_MemberSet]
    PRIMARY KEY CLUSTERED ([Mem_id] ASC);
GO

-- Creating primary key on [Ca_id] in table 'catagorySet'
ALTER TABLE [dbo].[catagorySet]
ADD CONSTRAINT [PK_catagorySet]
    PRIMARY KEY CLUSTERED ([Ca_id] ASC);
GO

-- Creating primary key on [Au_id] in table 'AuthorSet'
ALTER TABLE [dbo].[AuthorSet]
ADD CONSTRAINT [PK_AuthorSet]
    PRIMARY KEY CLUSTERED ([Au_id] ASC);
GO

-- Creating primary key on [Pub_id] in table 'PublisherSet'
ALTER TABLE [dbo].[PublisherSet]
ADD CONSTRAINT [PK_PublisherSet]
    PRIMARY KEY CLUSTERED ([Pub_id] ASC);
GO

-- Creating primary key on [Books_Book_id], [Member_Mem_id] in table 'BooksMember'
ALTER TABLE [dbo].[BooksMember]
ADD CONSTRAINT [PK_BooksMember]
    PRIMARY KEY CLUSTERED ([Books_Book_id], [Member_Mem_id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Books_Book_id] in table 'BooksMember'
ALTER TABLE [dbo].[BooksMember]
ADD CONSTRAINT [FK_BooksMember_Books]
    FOREIGN KEY ([Books_Book_id])
    REFERENCES [dbo].[BooksSet]
        ([Book_id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Member_Mem_id] in table 'BooksMember'
ALTER TABLE [dbo].[BooksMember]
ADD CONSTRAINT [FK_BooksMember_Member]
    FOREIGN KEY ([Member_Mem_id])
    REFERENCES [dbo].[MemberSet]
        ([Mem_id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BooksMember_Member'
CREATE INDEX [IX_FK_BooksMember_Member]
ON [dbo].[BooksMember]
    ([Member_Mem_id]);
GO

-- Creating foreign key on [Author_Au_id] in table 'BooksSet'
ALTER TABLE [dbo].[BooksSet]
ADD CONSTRAINT [FK_AuthorBooks]
    FOREIGN KEY ([Author_Au_id])
    REFERENCES [dbo].[AuthorSet]
        ([Au_id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AuthorBooks'
CREATE INDEX [IX_FK_AuthorBooks]
ON [dbo].[BooksSet]
    ([Author_Au_id]);
GO

-- Creating foreign key on [catagory_Ca_id] in table 'BooksSet'
ALTER TABLE [dbo].[BooksSet]
ADD CONSTRAINT [FK_Bookscatagory]
    FOREIGN KEY ([catagory_Ca_id])
    REFERENCES [dbo].[catagorySet]
        ([Ca_id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Bookscatagory'
CREATE INDEX [IX_FK_Bookscatagory]
ON [dbo].[BooksSet]
    ([catagory_Ca_id]);
GO

-- Creating foreign key on [Publisher_Pub_id] in table 'BooksSet'
ALTER TABLE [dbo].[BooksSet]
ADD CONSTRAINT [FK_BooksPublisher]
    FOREIGN KEY ([Publisher_Pub_id])
    REFERENCES [dbo].[PublisherSet]
        ([Pub_id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BooksPublisher'
CREATE INDEX [IX_FK_BooksPublisher]
ON [dbo].[BooksSet]
    ([Publisher_Pub_id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------